// MQTT client
#include <PubSubClient.h>
#define MQTT_broker "192.168.100.11"
#define MQTT_port 1883
#define MQTT_user ""
#define MQTT_password ""




// DEFINITION DES TOPICS POUR CE MODULE -------------------------------------------
char topic_lumiere[8 + DEVICEID_SIZE];
char topic_lumiere_color[8 + 6 + DEVICEID_SIZE];
char topic_lumiere_bright[8 + 11 + DEVICEID_SIZE];
char topic_lumiere_anim[8 + 10 + DEVICEID_SIZE];
char topic_lumiere_rempli[8 + 7 + DEVICEID_SIZE];
char topic_lumiere_allume_barres[8 + 7 + 7 + DEVICEID_SIZE];



WiFiClient espClient;
PubSubClient clientMQTT(espClient); // Definition du client MQTT


char g_CHAR_messageBuff[100];







// --------------------------------------------------------------------------------
// Reconnexion au serveur MQTT
//
void MQTT_connect() {

  DEBUG("Boucle jusqu'à obtenir une connexion");
  //Boucle jusqu'à obtenir une connexion
  while (!clientMQTT.connected()) {
    DEBUG("Connexion au serveur MQTT...");

    // ON arrive à se conecter au brocker MQTT
    if (clientMQTT.connect(HostName, MQTT_user, MQTT_password)) {
      DEBUG("OK");

    // Connection au brocker MQTT ratée
    } else {
      DEBUG("KO, erreur : ");
      DEBUG(clientMQTT.state());
      DEBUG(" On attend 5 secondes avant de recommencer");
      delay(5000);
    }
  }

  // Souscription aux topics
  clientMQTT.subscribe("lumiere/#");
}





// --------------------------------------------------------------------------------
// Déclenche les actions à la réception d'un message MQTT.
// lumiere/lampeADN [ON|OFF]               : Allumage de la barre de LEDS.
// lumiere/lampeADN/color [#RRVVBB]        : Changement de couleur des LEDS.
// lumiere/lampeADN/animation [1/2/3/4/5]  : Animation des LEDS.
//
void MQTT_callback(char* topic, byte* payload, unsigned int length) {

  // create character buffer with ending null terminator (string)
  char message[100];
  unsigned int i;
  for ( i = 0; i < length; i++) {
    message[i] = payload[i];
  }
  message[i] = '\0';

  


  // Traitement des topics
  // .....................................................................................
  Couleur c;
  if ( strcmp( topic, topic_lumiere ) ==0 )  {
    DEBUG("Detection du topics :" + String( topic_lumiere ));

    if ( String( message ) == "ON") {
      DEBUG("Allumage les leds");

    } else if ( String( message ) == "OFF") {
      DEBUG("Extinction des leds");
      LED_changeCouleurInverse( c, 1 );
    }
    

  // .....................................................................................
  } else if ( strcmp( topic, topic_lumiere_color) == 0) {
    DEBUG("Detection du topics :" + String( topic_lumiere_color ));

    // Test si on a une couleur RGB dans le message
    if ( LED_isAColor( message ) ) {
      // Définition de la couleur
      c = LED_ExtractRVB( message );
      DEBUG("Affichage de la couleur : " + String(c.R) + " " + String(c.V) + " " + String(c.B));


      // Changemnt des LEDS avec la couleur
      LED_changeCouleur( c, 10 );
    }



  // .....................................................................................
  } else if ( strcmp( topic, topic_lumiere_bright) == 0 ) {
    DEBUG("Detection du topics :" + String( topic_lumiere_bright ));

    // Test si on a bien une valeur numérique
    if ( LED_isADigit( message ) ) {
      DEBUG("Luminosite : " + String( message ));
      strip.setBrightness( String( message ).toInt() % 255 );
      strip.show();
    }



  // .....................................................................................
  } else if ( strcmp( topic, topic_lumiere_anim) ==0 ) {
    DEBUG("Detection du topics :" + String( topic_lumiere_anim ));
    DEBUG("Lancement de l'Animation avec le parametre :" + String( message ));
    LED_Animation(String( message ).toInt());


  // .....................................................................................
  } else if ( strcmp( topic, topic_lumiere_rempli) ==0 ) {
    DEBUG("Detection du topics :" + String( topic_lumiere_rempli ));
    DEBUG("Lancement du remplissage à :" + String( message ));
    LED_Remplissage(String( message ).toInt());


  // .....................................................................................
  } else if ( strcmp( topic, topic_lumiere_allume_barres) ==0 ) {
    DEBUG("Detection du topics :" + String( topic_lumiere_allume_barres ));
    DEBUG("Lancement du remplissage des " + String( message ) + " barres");
    Couleur color;
    color.R = 255;
    color.V = 255;
    color.B = 255;
    for(int i = 0; i < String( message ).toInt(); i++){
      LED_AllumeBaton(i,  color );
    }
  }
}



// --------------------------------------------------------------------------------
// Initialisation du brocker MQTT.
//
void MQTT_setup(){
  // Création du client MQTT
  DEBUG("Création du client MQTT");
  clientMQTT.setServer(MQTT_broker, MQTT_port);  // Configuration de la connexion au serveur MQTT

  DEBUG("fonction de callback");
  clientMQTT.setCallback(MQTT_callback);    // La fonction de callback qui est executée à chaque réception de message


  // Connection au Brocker MQTT
  DEBUG("Connection au Brocker MQTT");
  MQTT_connect();


  // Construction des topcs auxquels s'abonner.
  DEBUG("Construction des topcs auxquels s'abonner.");
  sprintf( topic_lumiere,         "lumiere/%s", DeviceID);
  sprintf( topic_lumiere_color,   "lumiere/color/%s", DeviceID);
  sprintf( topic_lumiere_bright,  "lumiere/brightness/%s", DeviceID);
  sprintf( topic_lumiere_anim,    "lumiere/animation/%s", DeviceID);
  sprintf( topic_lumiere_rempli,  "lumiere/allume/%s", DeviceID);
  sprintf( topic_lumiere_allume_barres,  "lumiere/allume/%s/barres", DeviceID);
}