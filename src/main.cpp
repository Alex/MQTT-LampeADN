// ---------------------------------------------------------------------------------------
// Programme permettant :
// - d'allumer/éteindre une barre de LEDS (branchée sur la PIN D1 ).
// - de changer la couleur des LEDS.
// - de modifier l'intensité des LEDS.
// - de lancer une animation des LEDS.
//
// Le programme se branche en WIFI sur un broker MQTT et réagi au topics :
// lumiere/lampeADN [ON|OFF]                 : Allumage de la barre de LEDS.
// lumiere/color/lampeADN (R,V,B)            : Changement de couleur des LEDS.
// lumiere/brightness/lampeADN value         : luminosité [0-255]
// lumiere/animation/lampeADN value          : ID de l'animation [0-6]
// lumiere/allume/lampeADN value             : Allummage des leds du bas vers le haut (en %) 0= aucune, 100= toutes
// ---------------------------------------------------------------------------------------
#include <Arduino.h>
//#define ModeDebug

const String firmwareActualVersion = "1.1.1";

#ifdef ModeDebug
  #define DEBUG(message) \
    Serial.print("[DEBUG:"); \
    Serial.print(__func__); \
    Serial.print("("); \
    Serial.print(__LINE__); \
    Serial.print(")]-> "); \
    Serial.println(message);
#else
  #define DEBUG(message);
#endif



#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager
WiFiManager wifiManager;


// Customisation du nom du module ESP
#define HOSTNAME "LampeADN-"
char HostName[16];

// Pour répondre au topic MQTT [portal|barreverticale|lampeADN]
#define DEVICEID_SIZE 15
char DeviceID[DEVICEID_SIZE] = "lampeADN";



int LED_COUNT = 66;


// LEDS
#include "my_leds.h"


// Définition d'une structure pouvant stocker le message provenant de MQTT
#include "my_MQTT.h"








// ***************************************************************************************
void setup() {
  #ifdef ModeDebug
    // initialisation de la liaison série (pour le moniteur) .........................
    Serial.begin(115200);
    delay(5000);  // On attend que le port serie soit initialisé
    Serial.println();
    Serial.flush();
  #endif

  DEBUG("OK, let's go **********************************************************************");
  DEBUG("Version firmware :" + String( firmwareActualVersion ));


  // initialisation de la liaison WIFI ..............................................
  /* Si la connexion échoue, on lance un Access Point (AP) qui est visible dans les réseaux WIFI
      Il faut alors se connecter avec un smarthpone sur l'AP pour configurer le Wifi, le NodeMCU
      reboot et se connect avec le SSID et mot de passe saisie.
  */
  snprintf(HostName, 16, HOSTNAME"%06X", (uint32_t)ESP.getChipId());  // Concaténation du HOSTNAME avec la fin de l'adresse MAC
  wifiManager.setDebugOutput(false); // false ->Pour ne plus avoir le mot de passe WIFI qui s'affiche.
  wifiManager.autoConnect(HostName, "123456789");
  
  #ifdef ModeDebug
    DEBUG("IP address: "); Serial.println(WiFi.localIP());
    DEBUG("HOSTNAME: " + String(HostName) );
  #endif



  // Create a MQTT client ..........................................................
  DEBUG("Create a MQTT client");
  MQTT_setup();


  // Initialisation des leds .....................................................
  DEBUG("Initialisation des leds");
  strip.begin();           // INITIALIZE NeoPixel strip object
  strip.show();            // Turn OFF all pixels ASAP
  strip.setBrightness(255);
  LED_Animation(5);
  DEBUG("************************** Tout est initialise");
}







// **********************************************************************************************************
// **********************************************************************************************************
unsigned long lastRecu = 0;
int numled = 0;
void loop() {
  // Test si la connection Wifi existe toujours ...................................
  if (WiFi.status() != WL_CONNECTED) {
    // Si on est déconnecté on tente de se reconnecter automatiquement avec les anciens settings.
    wifiManager.autoConnect();
  }



  // Test si la connection MQTT est toujours valide ..............................
  if (!clientMQTT.connected()) {
    Serial.println("OUPS, on est plus connecté au server MQTT--------------------------");

    //MQTT_connect();

    // On reboot
    ESP.restart();
  }
  clientMQTT.loop();



  // Traitement des Messages MQTT ...................................................
  // Tout est fait dans  MQTT_callback()
}