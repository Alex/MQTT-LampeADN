# MQTT Lampe ADN #
![Lampe ADN allumée](./illustrations/lampAllumee.JPG) Programme ESP8266 permettant de faire réagire une double colonne de leds (leds strip) en fonction d'un message MQTT envoyé par un brocker.

Le nombre de leds est paramétrable.

La partie brocker n'est pas inclus dans le présent programme.


# Fonctionnement #
## Connection Wifi ##
La connection wifi est gérée par l'extension **WIFI manager**.

Si la connexion échoue, on lance un Access Point (AP) qui est visible dans les réseaux WIFI
Il faut alors se connecter avec un smarthpone sur l'AP pour configurer le Wifi en lui saisissant son SSID et le Mot de passe, 
le NodeMCU reboot et se connecte avec le SSID et mot de passe saisie. Une fois, ces identifiants saisies, l'ESP8266 est déclaré sur le réseau et il ne sera plus nécessaire de les saisir.

Peux importe l'adresse IP de l'ESP8266, elle n'est pas utile pour recevoir des messages MQTT (sauf si un filtrage est mis en place au niveau du brocker, mais cette documentation ne couvre pas cette partie).

## MQTT ##
L'adresse IP du brocker MQTT doit être définie dans le fichier MQTT.h

Les messages MQTT interprétés doivent être dans les topics:

- lumiere/lampeADN [ON|OFF]                : Allumage de la barre de LEDS.
- lumiere/color/lampeADN (R,V,B)           : Changement de couleur des LEDS.
- lumiere/brightness/lampeADN value        : luminosité [0-255]
- lumiere/animation/lampeADN value         : ID de l'animation [0-6]
- lumiere/allume/lampeADN value            : Allummage des leds du bas vers le haut (en %) 0 = aucune, 100 = toutes










---
title: MQTT Lampe ADN
subtitle: Changeur de couleur LEDS dans la lampe ADN en fonction de messages MQTT
lang: fre
author: Alexandre PERETJATKO
language: C (Arduino)
hardware: Node MCU ESP8266
website: https://git.alex-design.fr/Alex/MQTT-leds-color.git
...